# RedLab/Hack. 
## May 2024. Кейс №1
### Olenin Leonid 

1. Поднять сервис PostgreSQL + Grafana в Docker из файла [docker-compose.yml](/docker-compose.yml) с помощью команды:  
```docker compose up```

2. Список требуемых модулей можно посмотреть в файле [pyproject.toml](/pyproject.toml). Установка модулей осуществляется командами:  
```pip install poetry```  
```poetry install```  

3. Загрузить данные в базу данных PostgreSQL можно с помощью скрипта [upload_data_to_postgres.py](src/etl/upload_data_to_postgres.py).   
В параметрах запуска скрипта можно указать путь к файлу с данными (по умолчанию путь: data/raw/metrics_collector.tsv)  
```python -m src.etl.upload_data_to_postgres data/raw/metrics_collector.tsv```

4. После загрузки данных и небольшого исследования [03_eda_from_postgres.ipynb](notebooks/03_eda_from_postgres.ipynb) опредилили, что набор данных содержит более 18 млн. записей по 723 различным метрикам.  

5. Для того, чтобы визуально определить, какие из метрик можно использовать для детекции аномалий, а какие можем исключить, можно запустить скрипт [create_stats_pictures.py](src/vizualization/create_stats_pictures.py), который по каждой из 723 метрик создаст отдельный файл в каталоге `reports/fig` (итого 723 файла):  
```python -m src.vizualization.create_stats_pictures.py```  

![01_list_of_files.png](reports/pics/01_list_of_files.png)  

6. Содержимое одного файла ![Apdex.png](reports/figs/Apdex.png )  

7. Перейдем в `Grafana` по адресу `localhost:3000`, добавим источник данных из PostgreSQL
Добавим следующие графики  
* `web_response`:
~~~sql
SELECT point AS "time", SUM(total_call_time) / SUM(call_count) AS "web_response" FROM stats WHERE name = 'HttpDispatcher' GROUP BY point
~~~

* `throughput`:
~~~sql
SELECT point AS "time", SUM(call_count) AS "call_count" FROM stats WHERE name = 'HttpDispatcher' GROUP BY point
~~~

* `errors`:
~~~sql
SELECT 
  i.time,
  SUM(i.err_count) / SUM(i.http_count) as err_rate
FROM (
  SELECT
    h.point as time,
    h.call_count as http_count,
    0 as err_count
    FROM stats as h
    WHERE name = 'HttpDispatcher'

    UNION

    SELECT 
      e.point,
      0,
      e.call_count
    FROM stats as e
    WHERE name = 'ErrorsallWeb'
) AS i
group by i.time
order by i.time
~~~

8. Посмотрим на данные на примере 22.04.2024:   
![02_grafana_anomaly.png](/reports/pics/02_grafana_anomaly.png)

Можно предположить, что в 15:11 была зафиксирована ошибка, упала пропускная способность приложения и возможно, как следствие, в 15:39 и 16:09 увеличился ответ сервера. Попробуем это детектировать  

9. Подготовка данных для обучения  

С помощью [05_prepare_data.ipynb](/notebooks/05_prepare_data.ipynb) из базы данных в  Postgres подготовим промежуточную таблицу по данным метрики `HttpDispatcher` со следующими признаками:  
* web_response
* throughput
* err_rate

![03_data_interim.png](/reports/pics/03_data_interim.png)

10. Определение аномалий  

С помощью [06_baseline.ipynb](/notebooks/06_baseline.ipynb) был произведен анализ данных  

Поскольку первая производная показывает скорость изменения функции, то принято  решение: используя квантильное окно, определять, какие значения производной являются аномалиями (т.е. показатель достаточно резко меняет свое значение)  

Экспериментальным путем были подобраны квантили 0.005 и 0.995 (чтобы аномалий было небольшое количество), данные с аномалиями были загружены в Postgres в таблицу `anomaly`  

После чего в Grafana добавлен еще один график для отображения аномалий:  
~~~sql 
SELECT point, SUM(web_response) as web_response_anomaly FROM anomaly GROUP BY point ORDER BY point
~~~

![04_grafana_anomaly.png](/reports/pics/04_grafana_anomaly.png)

11. Выбор метрики  

Метрика зависит от ширины окна. Чем больше передается окно, тем больше в этом окне может быть аномальных значений.  

Можно было бы возвращать долю аномальных значений в переданном окне, но тогда это был бы очень маленький процент.  

Поэтому вместо доли аномальных значений опять же используются значения первых производных - они суммируются по модулю для данных с аномальными значениями (напомню, что это были отобраны большие значения) и делятся на сумму по модулю всех первых производных из заданного окна  

Поскольку аномалия могла возникнуть по данным из колонки `web_response` либо `throughput`, поэтому я рассчитываю метрику для каждой из этих колонок и возвращаю максимальное значение.  

12. Запуск приложения  

Для предсказания аномалий необходимо запустить скрипт [predict.py](/src/model/predict.py), передав путь к обработанному датасету и указать период для получения данных (дату начала и дату окончания)  

Пример вызова команды:  
```python -m src.model.predict data/processed/data.csv '2024-04-22 13:48:00' '2024-04-22 17:22:00'```

![05_predict.png](/reports/pics/05_predict.png)  

13. В ноутбуке [07_ML.ipynb](/notebooks/07_ML.ipynb) можно ознакомиться с примером использования машинного обучения для детекции аномалий.   

Основной подход был в следующем:  
* Очищаем модель от выбросов (порог определения аномалий выбран визуально с помощью boxblot)
* Добавляем признаки на основании дат
* Обучаем `xgboost` на очищенных данных
* Выполняем предсказание на всех данных
* Получаем остатки `residuals` между реальным значением и предсказанным
* По определенному порогу фильтруем эти остатки и загружаем их в Postgres

