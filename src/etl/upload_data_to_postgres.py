import click
from pathlib import Path
import pandas as pd
from sqlalchemy import create_engine
from time import time

uri = "postgresql://postgres:postgres@localhost:5432/redlab"
engine = create_engine(uri)
engine.connect()

columns = ['account_id', 'name', 'point', 'call_count', 'total_call_time', 
           'total_exclusive_time', 'min_call_time', 'ax_call_time', 'sum_of_squares', 
           'instances', 'language', 'app_name', 'app_id', 'scope', 'host', 
           'display_host', 'pid', 'agent_version', 'labels']

def table_exist(table_name, engine):
    df = pd.read_sql(f"""
                    SELECT 
                        * 
                    FROM pg_catalog.pg_tables
                    WHERE schemaname = 'public'
                        AND tablename = '{table_name}'
                    """, con=engine)
    return len(df) > 0

@click.command()
@click.argument('input_data', type=click.Path(exists=True), default='data/raw/metrics_collector.tsv')
@click.argument('table_name', type=click.STRING, default='stats')
def upload_data(input_data: Path, table_name: str):
    """
    upload data to postgres by chanks
    """
    i = 0
    chunksize=100_000
    start = time()

    subset = ['account_id', 'name', 'point', 'call_count', 'total_call_time']

    for chunk in pd.read_csv(input_data, 
                                iterator=True, 
                                chunksize=chunksize,
                                names=columns, 
                                sep='\t', 
                                header=None, 
                                parse_dates=['point']):

        # create table
        if not table_exist(table_name, engine):
            chunk[subset].head(0).to_sql(name=table_name, con=engine, if_exists='replace')

        cur = time()

        chunk['name'] = chunk['name'].apply(lambda x: ''.join(filter(str.isalpha, x)))
        chunk[subset].to_sql(name=table_name, con=engine, if_exists='append', index=False)   

        i += chunksize

        print(f"processed {i} raw by {(time() - cur):.1f} seconds")
        
    print(f"processed by {(time() - start):.1f} seconds")

if __name__ == "__main__":
    upload_data()
