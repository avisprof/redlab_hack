import click
from pathlib import Path

import pandas as pd
import dateutil.parser as dparser

# Anomaly hyperparameters (quantiles)
Q_LOW = 0.005
Q_HIGH = 0.995

@click.command()
@click.argument('input_data', type=click.Path(exists=True), default='data/processed/data.csv')
@click.argument('start', type=click.STRING, required=True)
@click.argument('end', type=click.STRING, required=True)
def cli_predict(input_data: Path, start:str, end:str):


    print(f"Read dataframe: {input_data}")
    data = pd.read_csv(input_data, parse_dates=['point'],index_col='point')

    start_date = dparser.parse(start)
    end_date = dparser.parse(end)
    print(f"Apply filter from {start_date} to {end_date}")
    period = pd.date_range(start_date, end_date, freq='1min')

    data_filtered = data.reset_index()[data.reset_index()['point'].isin(period)]
    print(f"Filtered data: {len(data_filtered)} row")

    mask = get_anomaly_mask(data_filtered)
    df_anomaly = data_filtered[mask].copy().reset_index()
    df_anomaly_filtered = df_anomaly[df_anomaly['point'].isin(period)]
    print(f"Anomaly data: {len(df_anomaly_filtered)} row")

    metric_t = calc_metric(data_filtered, df_anomaly_filtered, 'throughput_diff')
    metric_w = calc_metric(data_filtered, df_anomaly_filtered, 'web_response_diff')
    score = max(metric_t, metric_w)
    print(f"Final score: {score}")
    return score

def calc_metric(data, data_anomaly, by_column):
    data_sum = abs(data[by_column]).sum()
    anomaly_sum = abs(data_anomaly[by_column]).sum()
    score = round(100 * anomaly_sum /data_sum, 1)
    print(f"score by column {by_column} = {score} (anomaly sum: {anomaly_sum:.3f} / data sum: {data_sum:.3f})")
    return score
    

def get_anomaly_mask(data):
    mask_web = get_anomaly(data, 'web_response')
    mask_thr = get_anomaly(data, 'throughput')
    mask_err = get_anomaly(data, 'err_rate')
    return mask_web | mask_thr | mask_err

def get_anomaly(data, column):
    column_diff = f'{column}_diff'
    low = data[column_diff].quantile(Q_LOW) 
    high = data[column_diff].quantile(Q_HIGH)
    mask = (data[column_diff]<low) | (data[column_diff]>high)
    return mask

if __name__ == '__main__':
    cli_predict()

