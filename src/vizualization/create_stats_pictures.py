import pandas as pd
import matplotlib.pyplot as plt
from sqlalchemy import create_engine
from tqdm import tqdm

uri = "postgresql://postgres:postgres@localhost:5432/redlab"
engine = create_engine(uri)

def get_data_save(name):

    df = pd.read_sql(f"select * from stats where name = '{name}'", con=engine)
    df['collector'] = df['total_call_time'] / df['call_count']
    
    index = 'point'
    targets = ['call_count', 'total_call_time', 'collector']
    df_agg = df.set_index(index,drop=True)
    
    fig, ax = plt.subplots(len(targets),1, figsize=(8,10))
    for i, target in enumerate(targets):
        df_agg[target].plot(ax=ax[i], style='.', ms=1)
        ax[i].set_title(target)
    
    fig.suptitle(name)
    fig.tight_layout()

    file_name = ''.join(filter(str.isalpha, name))
    if len(file_name) > 200:
        file_name = file_name[:200]+"_"+str(len(file_name))
        
    fig.savefig(f"reports/figs/{file_name}.png")

    df = None
    df_agg = None
    
    plt.close('all')

if __name__ == '__main__':

    engine.connect()

    metrics = pd.read_sql("select distinct(name) from stats;", con=engine)

    for metric_name in tqdm(metrics['name'].values):
        get_data_save(metric_name)

